#encoding: utf-8
ActiveAdmin.register Categoria do     
  index do        
    column :nom           
    column :descripcio
    default_actions                   
  end                                      

  form do |f|                         
    f.inputs "Categoria info" do       
      f.input :nom                  
      f.input :descripcio
    end                               
    f.buttons                         
  end                                 
end                                   
