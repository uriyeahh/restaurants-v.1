#encoding: utf-8
ActiveAdmin.register Plat do     
  index do        
    column :nom           
    column :descripcio
    column :foto
    column :categoria
    column :preu
    default_actions                   
  end                                      

  form do |f|                         
    f.inputs "Informació del plat" do       
      f.input :nom                  
      f.input :descripcio, :html_options => { :class => 'tiny_mce' }
      f.input :foto, :value => "http://distilleryimage6.s3.amazonaws.com/2864ccbc319811e2b8e822000a1fbcc7_7.jpg"
      f.input :categoria, :as => :select, :required => true, :collection => Hash[Categoria.all.map{|b| [b.nom,b.id]}]
      f.input :preu
    end                          
    f.buttons                         
  end                                 
end