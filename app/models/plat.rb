class Plat < ActiveRecord::Base
  attr_accessible :descripcio, :nom, :foto, :categoria_id, :preu

  belongs_to :categoria

  def name
  	nom
  end

end
