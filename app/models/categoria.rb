class Categoria < ActiveRecord::Base
  attr_accessible :descripcio, :nom

  has_many :plats

  def name
  	nom
  end
end
