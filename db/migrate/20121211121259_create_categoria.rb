class CreateCategoria < ActiveRecord::Migration
  def change
    create_table :categoria do |t|
      t.string :nom
      t.text :descripcio

      t.timestamps
    end
  end
end
