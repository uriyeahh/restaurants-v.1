class CreatePlats < ActiveRecord::Migration
  def change
    create_table :plats do |t|
      t.string :nom
      t.text :descripcio

      t.timestamps
    end
  end
end
